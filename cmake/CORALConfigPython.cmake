# Collection of helpers for CORAL python module configuration/installation

include(CORALConfigScripts)
function(coral_install_python_modules)
  # Copy and install python modules into the build and install areas
  file(GLOB _files RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/python python/*.py python/*/*.py python/*/*/*.py python/*/*/*.sql)
  if(NOT ${CMAKE_VERSION} VERSION_LESS 3.6.0)
    list(FILTER _files EXCLUDE REGEX "#")
  endif()
  foreach(_file ${_files})
    copy_to_build(${_file} python python)
    get_filename_component(_dstdir python/${_file} PATH)
    install(FILES ${CMAKE_BINARY_DIR}/python/${_file} DESTINATION ${_dstdir})
  endforeach(_file)
endfunction()
