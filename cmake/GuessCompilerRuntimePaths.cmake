# GuessCompilerRuntimePaths(lib_dirs_var bin_dirs_var)
#
# Find the compiler binary and library path.
#
function(GuessCompilerRuntimePaths lib_dirs_var bin_dirs_var)
  # Search standard libraries.
  set(std_library_path)
  if(CMAKE_HOST_UNIX)
    # Guess the LD_LIBRARY_PATH required by the compiler we use (only Unix).
    _find_standard_lib(libstdc++.so std_library_path)
    if (CMAKE_CXX_COMPILER MATCHES "icpc")
      _find_standard_lib(libimf.so icc_libdir)
      set(std_library_path ${std_library_path} ${icc_libdir})
    endif()
    # this ensures that the std libraries are in RPATH
    link_directories(${std_library_path})
    # find the real path to the compiler
    set(compiler_bin_path)
    get_filename_component(cxx_basename "${CMAKE_CXX_COMPILER}" NAME)
    foreach(_ldir ${std_library_path})
      while(NOT _ldir STREQUAL "/")
        get_filename_component(_ldir "${_ldir}" PATH)
        if(EXISTS "${_ldir}/bin/${cxx_basename}")
          set(compiler_bin_path ${compiler_bin_path} "${_ldir}/bin")
          break()
        endif()
      endwhile()
    endforeach()
  endif()
  set(${lib_dirs_var} ${std_library_path} PARENT_SCOPE)
  set(${bin_dirs_var} ${compiler_bin_path} PARENT_SCOPE)
  message(STATUS "Compiler lib path is ${std_library_path}")
  message(STATUS "Compiler bin path is ${compiler_bin_path}")
endfunction()

#-------------------------------------------------------------------------------
# _find_standard_libdir(libname var)
#
# helper function Find the location of a standard library asking the compiler.
#-------------------------------------------------------------------------------
function(_find_standard_lib libname var)
  #message(STATUS "find ${libname} -> ${CMAKE_CXX_COMPILER} ${CMAKE_CXX_FLAGS} -print-file-name=${libname}")
  set(_cmd "${CMAKE_CXX_COMPILER} ${CMAKE_CXX_FLAGS} -print-file-name=${libname}")
  separate_arguments(_cmd)
  execute_process(COMMAND ${_cmd} OUTPUT_VARIABLE cpplib)
  get_filename_component(cpplib ${cpplib} REALPATH)
  get_filename_component(cpplib ${cpplib} PATH)
  # Special hack for the way gcc is installed on AFS at CERN: but this must
  # be disabled as it fails for gcc on CVMFS! (CORALCOOL-2853, CORALCOOL-2916)
  ###string(REPLACE "contrib/gcc" "external/gcc" cpplib ${cpplib})
  #message(STATUS "${libname} lib dir -> ${cpplib}")
  set(${var} ${cpplib} PARENT_SCOPE)
endfunction()
