# Do not remove this line
if [ "1" != "1" ]; then echo "Force failure and exit if sourced from csh"; fi

# Software versions
LCG_version='LCG_75root6'
###GCCXML_version='0.9.0_20131026'
Python_version='2.7.9'
Python_twodigit='python2.7'
ROOT_version='6.02.08'
###xrootd_version='3.3.6'

# Platform (O/S and compiler)
LCG_platform=x86_64-slc6-gcc48-opt
gcc_version='4.8.1'

#-----------------------------------------------------------------------------

LCG_releases=/afs/cern.ch/sw/lcg/releases/$LCG_version

# Software installations
###GCCXML_home=$LCG_releases/gccxml/$GCCXML_version/$LCG_platform
Python_home=$LCG_releases/Python/$Python_version/$LCG_platform
ROOT_base=$LCG_releases/ROOT/$ROOT_version
ROOT_home=$ROOT_base/$LCG_platform
###xrootd_home=$LCG_releases/xrootd/$xrootd_version/$LCG_platform
gcc_home=$LCG_releases/gcc/$gcc_version/x86_64-slc6

# === OVERRIDE ROOT VERSION (test ROOT-5698 in dev3 master) ===
ROOT_base=/afs/cern.ch/sw/lcg/app/nightlies/dev3/Mon/ROOT/HEAD
ROOT_home=$ROOT_base/$LCG_platform
###ROOT_base=/afs/cern.ch/sw/lcg/app/nightlies/dev4/Mon/ROOT/v6-02-00-patches
###ROOT_home=$ROOT_base/$LCG_platform

# Set up compiler
export LD_LIBRARY_PATH=$gcc_home/lib64:$LD_LIBRARY_PATH
export COMPILER_PATH=$gcc_home/lib/gcc/x86_64-unknown-linux-gnu/$gcc_version
export PATH=$gcc_home/bin:$PATH

# Set up Python
export LD_LIBRARY_PATH=$Python_home/lib:$LD_LIBRARY_PATH
export MANPATH=$Python_home/share/man:$MANPATH
export PATH=$Python_home/bin:$PATH
export ROOT_INCLUDE_PATH=$Python_home/include/$Python_twodigit # First in path

# Set up gccxml (needed by ROOT)
###export MANPATH=$GCCXML_home/share/man:$MANPATH
###export PATH=$GCCXML_home/bin:$PATH

# Set up xrootd (needed by ROOT)
###export LD_LIBRARY_PATH=$xrootd_home/lib64:$LD_LIBRARY_PATH
###export PATH=$xrootd_home/bin:$PATH
###export ROOT_INCLUDE_PATH=$xrootd_home/include:$ROOT_INCLUDE_PATH

# Set up ROOT
export ROOTSYS=$ROOT_home
export LD_LIBRARY_PATH=$ROOT_home/lib:$LD_LIBRARY_PATH
export MANPATH=$ROOT_home/man:$MANPATH:$ROOT_base/src/root/man
export PATH=$ROOT_home/bin:$PATH
export PYTHONPATH=$ROOT_home/lib:$PYTHONPATH
