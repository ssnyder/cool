Package Utilities
Package managers: Sven A. Schmidt
Package contributors: Andrea Valassi, Marco Clemencic

==============================================================================
!2008.11.10 - Andrea

Internal doc for tag COOL_2_6_0.

Summary of changes in Utilities with respect to COOL_2_6_0-pre6:
- Disable compilation warnings (from Qt headers) for ACE on gcc43 (bug #42574).
- Fix VerificationClient to remove a gcc43 compilation warning.

==============================================================================
!2008.10.21 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_2_4_0b. Rebuild of the 2.4.0a release for the LCG_54h configuration.
Add support for Oracle on MacOSX. Bug fixes in CORAL and frontier_client.
No change in the source code. Software version remains "2.4.0".

NB: None of the _code_ changes in COOL_2_4-branch are released in COOL_2_4_0b!
NB: [there is one minor exception, RelationalCool/tests/RelationalDatabaseId]
NB: Only the _config_ branch changes are released in COOL_2_4_0b!

==============================================================================
!2008.10.16 - Andrea

Internal tag COOL_2_6_0-pre5-bis ('2008/10/16 10:00:00').

Changes in Utilities with respect to COOL_2_6_0-pre5:
- Fix bug #42184 (exceptions not caught in CoolDBDiscovery).
- Several enhancements in ACE (enable ChannelSelection, fix a few bugs).
- Link CORAL RelationalAccess only if needed 
  (not yet using NEEDS_CORAL macros).
- Start the port to gcc4.3. 
  Add missing STL headers to the implementation code.
  Add gcc43 to all .cvsignore files.

==============================================================================
!2008.09.27 - Andrea

Internal tag COOL_2_6_0-pre5 (-D '2008/09/27 10:00:00').

No changes in Utilities with respect to COOL_2_6_0-pre4.

==============================================================================
!2008.09.26 - Andrea

Internal tag COOL_2_6_0-pre4.

Summary of changes in Utilities with respect to COOL_2_6_0-pre3:
- Several enhancements in the ACE graphical editor tool.
- Fix bug #42184 (exceptions not caught in CoolDBDiscovery).

==============================================================================
!2008.09.11 - Andrea

Internal tag COOL_2_6_0-pre3.

No changes in Utilities with respect to COOL_2_6_0-pre2.

==============================================================================
!2008.09.01 - Andrea

Internal tag COOL_2_6_0-pre2.

No changes in Utilities with respect to COOL_2_6_0-pre1.

==============================================================================
!2008.08.27 - Andrea

Internal tag COOL_2_6_0-pre1.

Changes in Utilities with respect to COOL_2_5_0:
- Temporary workaround for Boost 1.35.1 bug (warnings from Boost headers).
- Add cmt/version.cmt to prepare support for the latest CMT version.

==============================================================================
!2008.06.10 - Andrea

Tag COOL_2_5_0. Production release with API semantic changes, API extensions
and package structure changes to remove the dependency on SEAL.
Upgrade to LCG_55 using the 'de-SEALed' CORAL_2_0_0.

Changes in Utilities with respect to COOL_2_4_0a:
- De-SEAL all utilities.
- Several enhancements in the ACE graphical editor tool.

==============================================================================
!2008.06.04 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_2_4_0a. Rebuild of the 2.4.0 release for the LCG_54g configuration.
Port to osx105 and .so shared lib names on MacOSX. Changes in CORAL and ROOT.
No change in the source code. Software version remains "2.4.0".

NB: None of the _code_ changes in COOL_2_4-branch are released in COOL_2_4_0a!
NB: Only the (osx105) _config_ branch changes are released in COOL_2_4_0a!

==============================================================================
!2008.04.11 - Marco

Remove dependency on SEAL.
- Use the new ctor and connectionSvc method of cool::Application and the 
  new class coral::ConnectionService instead of seal::Context.
- Replace seal::TimeInfo::sleep with a sleep function implemented using Boost.

==============================================================================
!2008.02.28 - Andrea

Tag COOL_2_4_0. Production release with backward compatible API extensions.
Upgrade to LCG_54b using CORAL_1_9_5.

==============================================================================
!2008.02.21 - Andrea

Tag COOL_2_3_1. Bug-fix production release (binary compatible with 2.3.0) 
with performance optimizations for multi-version retrieval and insertion.
Upgrade to LCG_54a including bug fixes in ROOT 5.18.00a. 

==============================================================================
!2008.02.19 - Andrea

Add the ACE editor from Chun Lik Tan (Alvin) and Murrough Landon.

==============================================================================
!2008.02.13 - Andrea (-D '2008/02/13 19:30:00')

Internal tag COOL_2_3_1-pre2 (bug fixes and performance optimizations).
Use the default LCG_54 (including ROOT 5.18) on all platforms.

==============================================================================
!2008.02.04 - Andrea

Internal tag COOL_2_3_1-pre1 (bug fixes and performance optimizations).

==============================================================================
!2008.01.21 - Andrea

Tag COOL_2_3_0. Production release with backward compatible API extensions.
Upgrade to LCG_54 using Python 2.5, ROOT 5.18 and several other new externals.

PyCool is not supported on MacOSX because of bug #32770 in ROOT 5.18.00.
The COOL nightlies are all green (except for the PyCool tests on MacOSX).

==============================================================================
!2007.12.19 - Andrea

Internal tag COOL_2_3_0-pre1 (new COOL API extensions on standard LCG_53f).
All CMT tests successful, bash tests not updated. Software version is 2.3.0.

==============================================================================
!2007.10.13 - Andrea

Tag COOL_2_2_2. Production release (binary compatible with 2.2.0) with many
performance and configuration improvements and bug fixes. New versions of 
CORAL and Frontier server (fixing all pending problems in the tests) and ROOT.
This is the first COOL release built by the SPI team (and no SCRAM config).

==============================================================================
!2007.11.08 - Andrea

Internal tag COOL_2_2_2-pre2 (using private CORAL192 build and SEAL193 copy).
All CMT tests successful, bash tests not updated. Software version is 2.2.2.

==============================================================================
!2007.11.07 - Andrea

Internal tag COOL_2_2_2-pre1 (using private CORAL192 build and SEAL193 copy).
All CMT tests successful (no pending Frontier failures).
Bash tests not updated (Wine failures are expected).

==============================================================================
!2007.10.30 - Andrea

Added a fake make target "examples" to please nmake (task #5414).

==============================================================================
!2007.10.29 - Marco

Added a fake make target "tests" to please nmake (task #5414).

==============================================================================
!2007.10.11 - Andrea

Tag COOL_2_2_1. Production release (binary compatible with 2.2.0) with many
configuration improvements, feature enhancements and bug fixes. New versions 
of CORAL (with important bug fixes for SQLite and Frontier), ROOT and SEAL.
This is the first COOL release with support for MacOSX/Intel platforms.

==============================================================================
!2007.10.08 - Andrea

Internal tag COOL_2_2_1-pre5. Last private tag before COOL_2_2_1.
All CMT/SCRAM tests successful on all platforms using a private CORAL191
(except for the last pending failure RO_02b1 - bug #23368 in FrontierAccess),
including MacOSX/PPC (last build) and gcc41 (not built for official COOL221).
Non-debug osx104_ia32_gcc401 not built, test results copied from debug version.

No changes in Utilities code or config.

==============================================================================
!2007.08.29 - Andrea (-D '2007/08/29 13:30:00')

Internal tag COOL_2_2_1-pre4.
First version with successful tests of slc4_ia32_gcc41 on SCRAM/BASH,
including SQLite and Frontier support and complete private config for CMT.

No changes in Utilities.

==============================================================================
!2007.08.24 - Andrea (-D '2007/08/24 17:15:00')

Internal tag COOL_2_2_1-pre3.
First version with successful tests of MacOSX Intel on both CMT/QMTEST 
and SCRAM/BASH, including PyCool (but still no Oracle support).
First version with successful tests of slc4_ia32_gcc41 on SCRAM/BASH
(but CMT is badly configured, and still no SQLite or Frontier support).

No changes in Utilities.

==============================================================================
!2007.08.07 - Andrea (-D '2007/08/07 16:02:00')

Internal tag COOL_2_2_1-pre2. 
First version with all QMTEST tests as successful as bash tests.

No changes in Utilities.

==============================================================================
!2007.08.02 - Andrea (-D '2007/08/03 14:05:00')

Internal tag COOL_2_2_1-pre1. 

No changes in Utilities.

==============================================================================
!2007.07.13 - Andrea

Tag COOL_2_2_0. Production release with many performance optimizations and bug 
fixes, also including backward-compatible API extensions and schema changes.
New versions of CMT, CORAL, ROOT/Reflex, oracle, sqlite, frontier_client
and LFC using LCG_53 (with respect to COOL 2.1.1 using LCG_51).

==============================================================================
!2007.04.16 - Marco

Tag COOL_2_1_1. Bug-fix production release (binary compatible with 2.1.0).
New versions of CORAL, ROOT and frontier_client using LCGCMT_51.

==============================================================================
!2007.03.24 - Andrea

Tag COOL_2_1_0. Production release with backward-compatible API extensions 
and schema changes. New versions of CORAL and ROOT using LCGCMT_50c.

==============================================================================
!2007.01.31 - Andrea

Tag COOL_2_0_0. Major production release with backward-incompatible
API and schema changes. New versions of SEAL, CORAL and frontier_client. 

==============================================================================
!2007.01.17 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_4. Production release (backward-compatible bug-fix, 
functionality enhancement and LCG_50 configuration upgrade release). 
Important fixes in SEAL (component model and multithreading), CORAL and ROOT.

==============================================================================
!2006.12.12 - Marco

Added a tool to extract the COOL databases in a given schema (coral db Id).
The tool is very simple: it does not check if the databases are accessible 
or not, it just finds the tables called xxx_DB_ATTRIBUTES (task #4163).

==============================================================================
!2006.10.30 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_3c. Rebuild of the 1.3.3 release for the LCG_48 configuration.
Important fixes in SEAL (component model and multithreading), CORAL and ROOT.
No change in the source code. Software version remains "1.3.3".

==============================================================================
!2006.10.16 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_3b. Rebuild of the 1.3.3 release for the LCG_47b configuration.
New version of ROOT. Pending bugs in SEAL and ROOT. 
Partial support for MySQL on Windows (pending PyCoolUtilities bug #20780). 
No change in the source code. Software version remains "1.3.3".

===============================================================================
!2006.09.29 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_3a. Rebuild release (bug fixes in CORAL and frontier_client).
Pending bugs in SEAL, new bugs in ROOT. Same source code as COOL_1_3_3. 
Only added two Frontier regression tests. Software version remains "1.3.3".

==============================================================================
!2006.08.28 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_3. Production release (backward-compatible bug-fix, 
functionality enhancement and configuration upgrade release).
Many important fixes in CORAL and Frontier; pending critical bugs in SEAL.

==============================================================================
!2006.07.12 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_2c. Rebuild of COOL_1_3_2 for the LCG_46 configuration.
No change in the source code. Software version remains "1.3.2".

==============================================================================
!2006.06.19 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_2b. Rebuild of COOL_1_3_2 for the LCG_45 configuration.
No change in the source code. Software version remains "1.3.2".

==============================================================================
!2006.05.14 - Andrea

Tag COOL_1_3_2a. Rebuild of the 1.3.2 release for the LCG_44 configuration.

==============================================================================
!2006.05.10 - Andrea

Tag COOL_1_3_2. Production release (backward-compatible 
bug-fix and Frontier support release in the 1.3 series).

Add David Front's VerificationClient to the COOL release installation.
Keep VerificationClient in its original CVS repository in the contrib area
and install it as a subdirectory of the Utilities package at boostrap time.
The only executable installed in the public PATH is coolVerificationClient.

==============================================================================
!2006.04.25 - Andrea, Marco, Sven

Tag COOL_1_3_1. Production release (backward-compatible 
COOL and CORAL bug-fix release in the 1.3 series).

==============================================================================
!2006.04.04 - Andrea

Tag COOL_1_3_0. Functionality enhancement production release (first 
release in the 1.3 series: backward incompatible API and schema changes).

==============================================================================
!2006.03.08 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_2_9 (non-HEAD branch after COOL_1_2_8).
Backward-compatible production release.
Same code as 1.2.8, but compiled against LCG_42_4.

==============================================================================
!2006.01.27 - Andrea

Tag COOL_1_2_8. Backward-compatible production release (internal migration 
from SEAL Reflex to ROOT Reflex; port to gcc344; attempted port to AMD64).

==============================================================================
!2006.01.16 - Andrea

Tag COOL_1_2_7. Backward-compatible production release
(internal migration from RAL to CORAL and from Reflection to seal Reflex).

==============================================================================
!2005.11.15 - Marco

Tag COOL_1_2_6. Production release (backward-compatible 
SEAL_1_7_6 and POOL_2_2_4 upgrade release in the 1.2 series).

==============================================================================
!2005.10.24 - Andrea, Marco

Tag COOL_1_2_5. Production release (backward-compatible 
SEAL_1_7_5 and POOL_2_2_3 upgrade release in the 1.2 series).

Retag COOL_1_2_4 code as COOL_1_2_5. 

==============================================================================
!2005.09.29 - Andrea, Sven, Marco, David, Uli

Tag COOL_1_2_4. Production release (backward-compatible 
bug-fix and POOL_2_2_1 upgrade release in the 1.2 series).

==============================================================================
!2005.08.29 - Andrea, Sven and Marco

Tag COOL_1_2_3. Production release (backward-compatible 
API enhancement and bug-fix release in the 1.2 series).

==============================================================================
!2005.07.27 - Andrea and Sven

Tag COOL_1_2_2. Production release (upgrade to SEAL_1_7_1 bug fix release).

==============================================================================
!2005.07.20 - Andrea and Sven

Tag COOL_1_2_1. Production release (backward-compatible 
API enhancement and bug-fix release in the 1.2 series).

==============================================================================
!2005.07.07 - Andrea and Sven

Internal tags for the next production release.

Tag COOL_1_2_1-pre1.

==============================================================================
!2005.06.28 - Andrea and Sven

Tag COOL_1_2_0. Production release (first release in the 1.2 series:
backward-incompatible API and schema changes).

==============================================================================
!2005.05.25 - Andrea and Sven

Tag COOL_1_1_0. Production release (first release in the 1.1 series:
repackaging and backward-incompatible API changes).

==============================================================================
!2005.05.09 - Andrea and Sven

Tag COOL_1_0_2. Production release (backward-compatible 
API enhancement and bug-fix release in the 1.0 series).

First production release using the new COOL CVS repository.

==============================================================================
!2005.05.02 - Andrea and Sven

Tag COOL_1_0_2-pre1. 

Internal release: same code as COOL_1_0_1, using the new COOL CVS repository.

==============================================================================
!2005.04.25 - Andrea and Sven

Tag COOL_1_0_1. Production release (bug fix release in the 1.0 series).

==============================================================================
!2005.04.21 - Andrea and Sven

Tag COOL_1_0_0. FIRST PRODUCTION RELEASE!

==============================================================================
!2005.04.21 - Andrea and Sven

Internal pre-release tags for the production release.

Tag COOL_1_0_0-pre3.
Tag COOL_1_0_0-pre2.
Tag COOL_1_0_0-pre1.

==============================================================================
!2005.03.20 - Andrea and Sven

Intermediate tags towards the production release.

Tag COOL_0_2_0-pre1.
Tag COOL_0_2_0-pre2.
Tag COOL_0_2_0-pre3.
Tag COOL_0_2_0-pre4.

==============================================================================
!2005.03.19 - Andrea and Sven

Intermediate tags towards the production release.
More details can be found in the RelationalCool release notes.

Tag COOL_0_1_1-pre1.
Tag COOL_0_1_1-pre2.

==============================================================================
!2005.03.03 - Andrea and Sven

This package contains a set of common COOL utilities.

Presently it is used for internal development it is not yet included in 
the public release. It is in any case tagged in sync with the COOL release.

Tagged as prerelease COOL_0_1_0-pre1.
Tagged as prerelease COOL_0_1_0-pre2 (only configuration changes).

==============================================================================
