From Andrea.Valassi@cern.ch Wed Apr  6 15:22:28 2005
Date: Wed, 6 Apr 2005 13:07:43 +0200 (CEST)
From: Andrea Valassi <Andrea.Valassi@cern.ch>
To: David Malon <malon@anl.gov>
Cc: Sven Schmidt <sas@abstracture.de>,
     LCG ConditionsDB developers
    <project-lcg-peb-conditionsdb-developers@cern.ch>
Subject: ~URGENT: user tags

Hi David,

a (hopefully) quick question. We are not going to implement the "user tag" 
functionality you requested in the production release. Nevertheless, I 
would like to be sure already that I understand EXACTLY what you require,
also because I'd like to foresee in advance the changes this implies to 
the IOV table schema, so that the IOV table schema in the production 
release is not radically incompatible with that required for implementing 
user tags.

I am including below two simple examples that, in my understanding,
demonstrate your requirement. I am actually including TWO examples for two
slightly different scenarios: 1. when a user can specify AT MOST ONE user
tag with each inserted IOV, and 2. when a user can specify ANY number of
user tags with each inserted IOV. I think that the simpler scenario 1
should satisfy your needs. I am rather reluctant to go to scenario 2
because it implies a more complex implementation (which I would solve by
increasing the storage overhead) and especially because it opens the door
to a potential unnecessary inflation in the number of user tags ("I'll
attach 15 different tags to this IOV, just in case I MAY need them
later"), with serious consequences on performance and scalability.
Of course user requirements come first, but I would invite users to ask 
for the requirements that are really required :-)

This is only meant to get a quick reply to get a feeling what you need: 
can you please have a quick look and confirm
- whether one of the two scenarios fulfills your requirements, or whether
  you actually had in mind something RADICALLY different
- if the first answer is yes, whether the first scenario is indeed
  enough or you would need the functionality of the second
Thanks a lot!

Feedback from anyone else (especially Atlas, since this where the 
requirement came from... Richard and Torre, for instance) is of course 
welcome! Please answer asap with a first quick feedback (today if you 
can?).

It's not a catastrophe if we dont converge now... but at least I'll 
give it a try to get a better idea before the production release.

	Cheers

		Andrea

Ps Sven, can you please prepare a picture for the two?

===============================================
User tag example specification (AV 06.04.2005)
===============================================

General idea: an IOV can be inserted with one (or more?) associated user tags. 
This means that a consistent HEAD version must be kept (within the folder
and the channel) not only for ALL IOVS, but also for EACH subset of IOVS
associated to any given user tag.

Let's consider two scenarios:
1. Each IOV can be inserted with ZERO or ONE user tags.
2. Each IOV can be inserted with any number of associated tags.

NB Each tag is independent of each other: it is NOT foreseen that one can 
ask for "the HEAD for tag A AND tag B, or the HEAD for tag A OR tag B".
Such a functionality can be implemented in scenario 2 by attaching ALSO 
tag A_or_B to any IOV satisfying either tagA or tagB, for instance (any 
such operation would be left to the user).

Example 1 (scenario 1)
----------------------
1. User inserts P1 payload in [  0, 100] with NO user tags
2. User inserts P2 payload in [ 10,  50] with user tag #1
3. User inserts P3 payload in [ 30,  80] with user tag #2
4. User inserts P4 payload in [ 20,  40] with user tag #1
5. User inserts P5 payload in [ 30,  70] with user tag #2

HEAD versions after last insertion:

All IOVS (irrespective of tags):
  P1 in [  0,  10]
  P2 in [ 10,  20]
  P4 in [ 20,  30]
  P5 in [ 30,  70]
  P3 in [ 70,  80]
  P1 in [ 80, 100]

User tag #1:
  P2 in [ 10,  20]
  P4 in [ 20,  40]
  P2 in [ 40,  50]

User tag #2:
  P5 in [ 30,  70]
  P3 in [ 70,  80]

Example 2 (scenario 2)
----------------------
1. User inserts P1 payload in [  0, 100] with NO user tags
2. User inserts P2 payload in [ 10,  50] with user tag #1
3. User inserts P3 payload in [ 30,  80] with user tag #2
4. User inserts P4 payload in [ 20,  40] with user tags #1 and #2 
5. User inserts P5 payload in [ 30,  70] with user tag #2

The ONLY difference with example 1 is that P4 is ALSO associated to tag #2.

HEAD versions after last insertion:

All IOVS (irrespective of tags): - no difference with example 1
  P1 in [  0,  10]
  P2 in [ 10,  20]
  P4 in [ 20,  30]
  P5 in [ 30,  70]
  P3 in [ 70,  80]
  P1 in [ 80, 100]

User tag #1: - no difference with example 1
  P2 in [ 10,  20]
  P4 in [ 20,  40]
  P2 in [ 40,  50]

User tag #2:
  P4 in [ 20,  30] - ONLY difference with respect to example 1
  P5 in [ 30,  70]
  P3 in [ 70,  80]

-------------------------------------------------------------------------------

From: RD Schaffer <R.D.Schaffer@cern.ch> 
To: Sven A. Schmidt <sas@abstracture.de> 
CC: Marco Clemencic <Marco.Clemencic@cern.ch>, 
    Andrea Valassi <Andrea.Valassi@cern.ch>, 
    Antoine Pérus <perus@lal.in2p3.fr>, 
    Richard Hawkings <rhawking@mail.cern.ch> 
Date: May 19 2005 - 5:04pm 

Hi there,

  Andrea should respond here. But I believe that his thoughts about 
David M.'s request is to provide user tags which are different than 
the regular tag. So one could insert with a user tag, and then later 
based on a particular user tag, tag a set of objects. This would resolve 
the problem of the 'in between' object - it would have a different user 
tag if so desired.

                        see you, RD

-------------------------------------------------------------------------------

For reference: David Malon presented his use case in the presentation 
at the 2003 CondDB Workshop: http://agenda.cern.ch/fullAgenda.php?ida=a036470


