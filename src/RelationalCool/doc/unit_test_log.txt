Passing tests on a given date. Failures in square brackets.

                     2004-11-30  2004-12-01  2004-12-02  2004-12-05
RalDatabase               4           5           9          12
RalDatabaseSvc            2           2           2           3
RelationalDatabase        1           1           1           1
RelationalFolder          1           2           2           2

                     2004-12-13  2004-12-15  2004-12-15
RalDatabase              12          15          19
RalDatabaseSvc            2           3           3
RelationalDatabase        1           1           1
RelationalFolder          1           4           6

                     2005-01-03  2005-01-14  2005-01-20  2005-01-26
RalDatabase              20          23          23          23
RalDatabaseSvc            3           4           4           4
RelationalDatabase        1           1           1           1
RelationalFolder          6           6           7           7

                     2005-02-02  COOL_0_0_3-pre1  2005-02-04
RalDatabase              29          29               29
RalDatabaseSvc            4           4                4
RelationalDatabase        1           1                1
RelationalFolder          8           8                8
HvsPathHandler            5           5                5

                     2005-02-13  2005-02-15  2005-02-21  2005-03-01
RalDatabase              59          32          32          32
RalDatabase_versioning               29          29          51
RalDatabaseSvc            4           4           4           4
RelationalDatabase        1           1           1           1
RelationalFolder          8           8           8           8
HvsPathHandler            5           5           5           5

                     2005-03-08  2005-03-17  2005-03-27  2005-04-04
RalDatabase              32          33          33          36
RalDatabase_versioning   51          51          58          60
RalDatabaseSvc            4           4           4           4
RelationalDatabase        1           1           1           1
RelationalFolder          8          12          17          17
HvsPathHandler            5           5           5           5

                     2005-05-06
RalDatabase              52    
RalDatabase_versioning   61
RalDatabase_extendedSpec 12    
RalDatabaseSvc            4  
RelationalDatabase        6 
RelationalFolder         23
HvsPathHandler            5
ObjectId                 11

                     2005-07-05
RalDatabase              57
RalDatabase_versioning   66
RalDatabase_extendedSpec 11    
RalDatabaseSvc            4  
RelationalDatabase        6 
RelationalFolder         30
RelationalFolderSet       4
RelationalTypeConverter   1
HvsPathHandler            5
ObjectId                 11
utility_methods           6
----------------------------
total                   201

