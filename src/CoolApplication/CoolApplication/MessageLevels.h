#ifndef COOLAPPLICATION_MESSAGELEVELS_H
#define COOLAPPLICATION_MESSAGELEVELS_H 1

// Header was moved to CoolKernel in COOL 2.6.0 to remove cyclic dependencies
// The CoolApplication copy is kept for backward compatibility in the user API
#include "CoolKernel/MessageLevels.h"

#endif // COOLAPPLICATION_MESSAGELEVELS_H
