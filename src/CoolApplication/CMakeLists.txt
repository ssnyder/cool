# Required external packages
find_package(CORAL REQUIRED)
include_directories(${CORAL_INCLUDE_DIRS})

include(CORALSharedLib)
CORALSharedLib(LIBS lcg_RelationalCool)
