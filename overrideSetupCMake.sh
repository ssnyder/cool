CORAL_base=
if [ "$HOSTNAME" == "buildcoverity.cern.ch" ]; then
  CORAL_base=/builda/Persistency/CORAL/trunk
elif [ "$HOSTNAME" == "moonshot-arm64-04" ] || [ `hostname -i` == "128.142.167.75" ]; then
  CORAL_base=/home/avalassi/CORAL/trunk
elif [ "$USER" == "avalassi" ]; then
  CORAL_base=/home/avalassi/CORAL/trunk
fi
export CMAKE_PREFIX_PATH=$CORAL_base/$BINARY_TAG:$CMAKE_PREFIX_PATH
